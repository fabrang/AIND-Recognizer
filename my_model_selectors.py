import math
import statistics
import warnings

import numpy as np
from hmmlearn.hmm import GaussianHMM
from sklearn.model_selection import KFold
from asl_utils import combine_sequences


class ModelSelector(object):
    '''
    base class for model selection (strategy design pattern)
    '''

    def __init__(self, all_word_sequences: dict, all_word_Xlengths: dict, this_word: str,
                 n_constant=3,
                 min_n_components=2, max_n_components=10,
                 random_state=14, verbose=False):
        self.words = all_word_sequences
        self.hwords = all_word_Xlengths
        self.sequences = all_word_sequences[this_word]
        self.X, self.lengths = all_word_Xlengths[this_word]
        self.this_word = this_word
        self.n_constant = n_constant
        self.min_n_components = min_n_components
        self.max_n_components = max_n_components
        self.random_state = random_state
        self.verbose = verbose

    def select(self):
        raise NotImplementedError

    def base_model(self, num_states):
        # with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        # warnings.filterwarnings("ignore", category=RuntimeWarning)
        try:
            hmm_model = GaussianHMM(n_components=num_states, covariance_type="diag", n_iter=1000,
                                    random_state=self.random_state, verbose=False).fit(self.X, self.lengths)
            if self.verbose:
                print("model created for {} with {} states".format(self.this_word, num_states))
            return hmm_model
        except:
            if self.verbose:
                print("failure on {} with {} states".format(self.this_word, num_states))
            return None


class SelectorConstant(ModelSelector):
    """ select the model with value self.n_constant

    """

    def select(self):
        """ select based on n_constant value

        :return: GaussianHMM object
        """
        best_num_components = self.n_constant
        return self.base_model(best_num_components)


class SelectorBIC(ModelSelector):
    """ select the model with the lowest Bayesian Information Criterion(BIC) score

    http://www2.imm.dtu.dk/courses/02433/doc/ch6_slides.pdf
    Bayesian information criteria: BIC = -2 * logL + p * logN
    """

    def select(self):
        """ select the best model for self.this_word based on
        BIC score for n between self.min_n_components and self.max_n_components

        references for num params caluclation:
         https://stats.stackexchange.com/questions/12341/number-of-parameters-in-markov-model
         http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.58.6208&rep=rep1&type=pdf
        :return: GaussianHMM object
        """
        warnings.filterwarnings("ignore", category=DeprecationWarning)

        # TODO implement model selection based on BIC scores
        best_bic = float('inf')
        best_model = None
        try:
            for num_states in range(self.min_n_components, self.max_n_components + 1):

                model = self.base_model(num_states)
                loglike = model.score(self.X, self.lengths)

                num_data_points = sum(self.lengths)
                num_params = num_states ** 2 + 2 * num_states * num_data_points - 1

                bic = -2 * loglike + num_params * np.log(num_data_points)

                if bic < best_bic:
                    best_bic = bic
                    best_model = model


        except Exception as e:
            print(e)

        return best_model


class SelectorDIC(ModelSelector):
    ''' select best model based on Discriminative Information Criterion

    Biem, Alain. "A model selection criterion for classification: Application to hmm topology optimization."
    Document Analysis and Recognition, 2003. Proceedings. Seventh International Conference on. IEEE, 2003.
    http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.58.6208&rep=rep1&type=pdf
    https://pdfs.semanticscholar.org/ed3d/7c4a5f607201f3848d4c02dd9ba17c791fc2.pdf
    DIC = log(P(X(i)) - 1/(M-1)SUM(log(P(X(all but i))
    loc(P(Xi)) == score function
    '''

    def select(self):
        warnings.filterwarnings("ignore", category=DeprecationWarning)

        rest_words = [self.hwords[word] for word in self.words if self.words != self.this_word]
        best_dic = float('-inf')
        best_model = None

        for num_states in range(self.min_n_components, self.max_n_components + 1):
            model = self.base_model(num_states)
            try:
                loglike = model.score(self.X, self.lengths)

                rest_loglikes = [model.score(word[0], word[1]) for word in rest_words]
                mean_log = np.mean(rest_loglikes)

                dic = loglike - mean_log
                # print("dic: " + str(dic) + 10*("*") + "loglike: " +str(loglike) +  10*("*") + "rest loglike: " + str(mean_log))

                if dic > best_dic:
                    best_dic = dic
                    best_model = model
                    # print("BEST ONE:" + str(best_dic))

            except Exception as e:
                print(e)

        return best_model


class SelectorCV(ModelSelector):
    ''' select best model based on average log Likelihood of cross-validation folds
    reference: https://github.com/letyrodridc/Udacity-AIND-Recognizer/blob/master/my_model_selectors.py

    '''

    def select(self):
        warnings.filterwarnings("ignore", category=DeprecationWarning)

        best_score = float('-inf')
        best_num_state = None
        for num_states in range(self.min_n_components, self.max_n_components + 1):
            score = 0
            count = 0
            average_score = float('-inf')
            if len(self.sequences) >= 2:
                split_method = KFold(n_splits=min(len(self.sequences), 3), shuffle=True)
                for cv_train_idx, cv_test_idx in split_method.split(self.sequences):
                    X_train, length_train = combine_sequences(cv_train_idx, self.sequences)
                    # print("length_train: " + str(self.lengths))
                    X_test, length_test = combine_sequences(cv_test_idx, self.sequences)

                    try:
                        model = GaussianHMM(num_states, covariance_type="diag", n_iter=1000,
                                            random_state=self.random_state, verbose=False).fit(X_train, length_train)
                        loglike = model.score(X_test, length_test)
                        count += 1
                        score = score + loglike
                    except Exception as e:
                        print(e)

                if count > 0:
                    average_score = score / count

            else:
                try:
                    model = GaussianHMM(num_states, covariance_type="diag", n_iter=1000,
                                        random_state=self.random_state, verbose=False).fit(self.X, self.lengths)
                    average_score = model.score(self.X, self.lengths)
                except Exception as e:
                    print(e)

            if average_score > best_score:
                best_score = average_score
                best_num_state = num_states
                print("Best score: " + str(best_score) + " achieved in num state: " + str(num_states))

        return self.base_model(best_num_state)
