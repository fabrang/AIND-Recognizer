import warnings
from asl_data import SinglesData


def recognize(models: dict, test_set: SinglesData):
    """ Recognize test word sequences from word models set

   :param models: dict of trained models
       {'SOMEWORD': GaussianHMM model object, 'SOMEOTHERWORD': GaussianHMM model object, ...}
   :param test_set: SinglesData object
   :return: (list, list)  as probabilities, guesses
       both lists are ordered by the test set word_id
       probabilities is a list of dictionaries where each key a word and value is Log Liklihood
           [{SOMEWORD': LogLvalue, 'SOMEOTHERWORD' LogLvalue, ... },
            {SOMEWORD': LogLvalue, 'SOMEOTHERWORD' LogLvalue, ... },
            ]
       guesses is a list of the best guess words ordered by the test set word_id
           ['WORDGUESS0', 'WORDGUESS1', 'WORDGUESS2',...]
   """
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    probabilities = []
    guesses = []
    # TODO implement the recognizer

    for i in range(len(test_set.get_all_Xlengths())):
        max_score = float("-inf")
        prop_dict = {}
        word_guess = None

        X, length = test_set.get_item_Xlengths(i)

        for word, model in models.items():
            try:
                # print(word)
                score = model.score(X, length)
                # print(score)
                prop_dict[word] = score
                # print(prop_dict)

                if score > max_score:
                    max_score = score
                    print(word)
                    word_guess = word

            except Exception as e:
                print(e)

        probabilities.append(prop_dict)
        guesses.append(word_guess)

    return (probabilities, guesses)
